#!/usr/bin/perl -w
#
# see http://search.cpan.org/~atrak/NetPacket-0.04/

use strict;

BEGIN {
	push @INC,"perl";
	push @INC,"build/perl";
	push @INC,"NetPacket";
};

use nfqueue;

use NetPacket::IP qw(IP_PROTO_TCP IP_PROTO_UDP);
use NetPacket::TCP;
use NetPacket::UDP;
use Socket qw(AF_INET AF_INET6);
use Mojo::mysql;
use JSON::XS;
use NetAddr::IP;
use Data::Structure::Util qw( unbless );
use List::Util qw( first );

use Data::Dumper;

my $mysql = Mojo::mysql->strict_mode('mysql://pifence:pifence@localhost/pifence');


my @queries;
my @IPWhiteList;
my @DNSWhiteList;
my $q;
my $counter = 0;

my $amazonJSONFile = '/home/pi/piFence/ip-ranges.json';
my $amazonJSONText = do {
   open(my $json_fh, "<:encoding(UTF-8)", $amazonJSONFile)
      or die("Can't open \$filename\": $!\n");
   local $/;
   <$json_fh>
};

my $amzJSON = JSON::XS->new;
my $amazonData = $amzJSON->decode($amazonJSONText);


sub cleanup()
{
	print "unbind\n";
	$q->unbind(AF_INET);
	print "close\n";
	$q->close();
}

sub getDNS
{
	my $dst_ip = shift;
	use Socket;
	my $iaddr = inet_aton($dst_ip); # or whatever address
	my $dnsname  = gethostbyaddr($iaddr, AF_INET) || "";
	return $dnsname;
}

sub updateDNSWhitelist()
{
	#UPDATE
	my $whitelisted = $mysql->db->query('select dns from whitelist where type = \'DNS\'');
	my @newWhitelist = ();
	while (my $next = $whitelisted->hash) {
		push @newWhitelist, $next->{dns};
	}
	@DNSWhiteList = @newWhitelist;
	print "Updated DNS whitelist array\n";
	print "-----------------------\n";
	#warn Dumper(@DNSWhiteList);
}

sub updateIPWhitelist()
{
	#UPDATE
	my $whitelisted = $mysql->db->query('select INET_NTOA(ip) as ip from whitelist where type = \'IP\'');
	my @newWhitelist = ();
	while (my $next = $whitelisted->hash) {
		push @newWhitelist, $next->{ip};
	}
	@IPWhiteList = @newWhitelist;
	print "Updated IP whitelist array\n";
	print "-----------------------\n";
	#warn Dumper(@whitelisted);
}

sub amazonWhitelist
{
	my $ip = shift;
	my $netIP = new NetAddr::IP->new($ip);
	my $boolean = 0;
	#print Dumper($data);
	for ( @{$amazonData->{'prefixes'}} ) {
		if(new NetAddr::IP->new($_->{'ip_prefix'})->contains($netIP))
		{
			$boolean = 1;
		}
	# print $_->{'ip_prefix'}."\n";
	}
	return $boolean;
}

sub cb()
{
	my ($payload) = @_;
	my $src_ip = "";
	my $dst_ip = "";
	my $geoData;
	my $dst_port;
	my $protocol = 'OTHER';
	my $Data;
	my $dnsname;

	#print "Perl callback called!\n";
	#my $tx = $mysql->db->begin;
	if ($payload) {
		#print "len: " . $payload->get_length() . "\n";

		my $ip_obj = NetPacket::IP->decode($payload->get_data());
		#print("$ip_obj->{src_ip} => $ip_obj->{dest_ip} $ip_obj->{proto}\n");
		
		#print Dumper($ip_obj);
		#print("$ip_obj->{src_ip} => $ip_obj->{dest_ip} $ip_obj->{proto}\n");
		#print "Id: " . $payload->swig_id_get() . "\n";
		$src_ip = $ip_obj->{src_ip};
		$dst_ip = $ip_obj->{dest_ip};

		#IGNORE THE ECHO AND IT WORKS....

		print "Src: ".$src_ip." Dst: ".$dst_ip."\n";
=head
		if ($src_ip eq '192.168.1.143')
		{
			if ( first { $_ eq $dst_ip } @IPWhiteList )
			{
				#print " ACCEPTED IP WHITELIST\n";
				$payload->set_verdict($nfqueue::NF_ACCEPT);
				return;
			}
			if(amazonWhitelist($dst_ip))
			{
				$payload->set_verdict($nfqueue::NF_ACCEPT);
				return;
			}
			$payload->set_verdict($nfqueue::NF_ACCEPT);
			return;
		}
   		#if(amazonWhitelist($dst_ip))
		#{
	#		print " AMAZON WHITELIST\n";
	#		$payload->set_verdict($nfqueue::NF_ACCEPT);
#			return;
#		}
=cut
		if ( first { $_ eq $dst_ip } @IPWhiteList )
		{
			#print " ACCEPTED IP WHITELIST\n";
			$payload->set_verdict($nfqueue::NF_ACCEPT);
			return;
		}
		if($ip_obj->{dest_ip} eq '192.168.1.1' || $ip_obj->{dest_ip} eq '255.255.255.255')
		{
			#Broadcast and router needs to be allowed (DHCP)
			#print " ACCEPTED ROUTER\n";
			$payload->set_verdict($nfqueue::NF_ACCEPT);
			return;
		}

		$dnsname = getDNS($dst_ip);
		if(first { index($dnsname, $_) != -1 } @DNSWhiteList)
		{
			#print " $dnsname - ACCEPTED DNS WHITELIST\n";
			$payload->set_verdict($nfqueue::NF_ACCEPT);
			return;
		}
		else
		{
			if($ip_obj->{proto} == IP_PROTO_TCP) {
				# decode the TCP header
				my $tcp_obj = NetPacket::TCP->decode($ip_obj->{data});
				print "TCP DENIED\n";
				#print "TCP src_port: $tcp_obj->{src_port}\n";
				#print "TCP dst_port: $tcp_obj->{dest_port}\n";
				$dst_port = $tcp_obj->{dest_port};
				my $ip_strip = NetPacket::TCP::strip($payload->get_data());

				#print Dumper($payload->get_data());
				$protocol = 'TCP';
			}
			elsif($ip_obj->{proto} == IP_PROTO_UDP) {
				# decode the UDP header
				my $udp_obj = NetPacket::UDP->decode($ip_obj->{data});
				print "UDP DENIED\n";
				#print "UDP src_port: $udp_obj->{src_port}\n";
				#print "UDP dst_port: $udp_obj->{dest_port}\n";
				$dst_port = $udp_obj->{dest_port};
				my $ip_strip = NetPacket::UDP::strip($payload->get_data());

				#print Dumper($ip_strip);
				$protocol = 'UDP';
			}
			else
			{
				#OTHER, WE WILL ALLOW THIS.
				print "OTHER ALLOWED.\n";
				$payload->set_verdict($nfqueue::NF_ACCEPT);
				return;
			}
			
			use Geo::IP;
			#my $gi = Geo::IP->new(GEOIP_MEMORY_CACHE);
			my $gi = Geo::IP->open("/usr/share/GeoIP/GeoIPCity.dat", GEOIP_STANDARD);
			# look up IP address '24.24.24.24'
			# returns undef if country is unallocated, or not defined in our database
			my $record = $gi->record_by_addr($dst_ip);
			if($record)
			{
					unbless($record);
					$geoData = encode_json($record);
					#print Dumper($geoData);
			}

			#$mysql->db->query(
			#'INSERT INTO traffic (src_ip, dst_ip, dst_dns, dst_geo, timestamp, dst_port, protocol) 
			#	VALUES (TRIM(INET_ATON(?)), TRIM(INET_ATON(?)), ?, ?, now(), ?, ?) on duplicate key update
			#	timestamp = NOW(), dst_geo = ?', ($src_ip, $dst_ip, $dnsname, $geoData, $dst_port, $protocol, $geoData));
			$Data = {
				'src_ip' => $src_ip,
				'dst_ip' => $dst_ip,
				'dnsname' => $dnsname,
				'geoData' => $geoData,
				'dst_port' => $dst_port,
				'protocol' => $protocol
			};
			print " DNS: ".$dnsname." PORT: ".$dst_port." PROTOCOL: ".$protocol." - DROP\n";
			#my $query = "INSERT INTO traffic (src_ip, dst_ip, dst_dns, dst_geo, timestamp, dst_port, protocol) 
			#	VALUES (TRIM(INET_ATON($src_ip)), TRIM(INET_ATON($dst_ip)), '$dnsname', '$geoData', now(), $dst_port, $protocol) on duplicate key update
			#	timestamp = NOW(), dst_geo = $geoData";
			push(@queries, $Data);
			$payload->set_verdict($nfqueue::NF_DROP);
		}
	}
	else
	{
		print "\n---NOPAYLOAD---\n";
		$payload->set_verdict($nfqueue::NF_ACCEPT);
	}
	#$tx->commit;
	#print "COUNT : : : ". scalar @queries."\n";

	$counter++;
	if ($counter >= 20)
	{
		print "COUNTER: ".$counter."\n";
		eval {
			my $tx = $mysql->db->begin;
			for( my $i = 0; $i < scalar @queries; $i++)
			{
				my $insert = shift @queries;
				
			 	#print Dumper($insert);
				$mysql->db->query(
					'INSERT INTO traffic (src_ip, dst_ip, dst_dns, dst_geo, timestamp, dst_port, protocol) 
					VALUES (TRIM(INET_ATON(?)), TRIM(INET_ATON(?)), ?, ?, now(), ?, ?) on duplicate key update
					timestamp = NOW(), dst_geo = ?, counter = (select t1.counter from traffic as t1 where t1.src_ip = TRIM(INET_ATON(?)) and t1.dst_ip = TRIM(INET_ATON(?)))+1', 
					(
						$insert->{'src_ip'}, 
						$insert->{'dst_ip'}, 
						$insert->{'dnsname'}, 
						$insert->{'geoData'}, 
						$insert->{'dst_port'}, 
						$insert->{'protocol'}, 
						$insert->{'geoData'},
						$insert->{'src_ip'}, 
						$insert->{'dst_ip'}
					)
				);
			}
			$tx->commit;
		};
		#print "======COMMIT======\n";
		print $@ if $@;
		#print "======COMMIT======\n";
		updateIPWhitelist();
		updateDNSWhitelist();
		$counter = 0;
	}
}

$q = new nfqueue::queue();

if( scalar @IPWhiteList == 0 )
{
	updateIPWhitelist();
	updateDNSWhitelist();
}

$SIG{INT} = "cleanup";

print "setting callback\n";
$q->set_callback(\&cb);

print "open\n";
$q->fast_open(0, AF_INET);

$q->set_queue_maxlen(5000);

print "trying to run\n";
$q->try_run();

