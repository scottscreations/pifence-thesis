CREATE TABLE
    `pifence`.users
    (
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        username VARCHAR(255) NOT NULL,
        hash TEXT NOT NULL,
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8mb4

CREATE TABLE
`pifence`.traffic
(
    dst_geo TEXT,
    TIMESTAMP TIMESTAMP,
    dst_dns TEXT,
    dst_ip INT(10) UNSIGNED NOT NULL,
    src_ip INT(10) UNSIGNED NOT NULL,
    dst_port INT,
    protocol ENUM('UDP', 'TCP', 'OTHER'),
    counter INT UNSIGNED DEFAULT 0 NOT NULL,
    CONSTRAINT traffic_pk PRIMARY KEY (dst_ip, src_ip),
    CONSTRAINT traffic_ix1 UNIQUE (dst_ip, src_ip)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4

CREATE TABLE
    `pifence`.whitelist
    (
        ip INT(10) UNSIGNED,
        dns VARCHAR(255),
        geo TEXT,
        type ENUM('IP','DNS') DEFAULT 'IP' NOT NULL,
        created_on TIMESTAMP DEFAULT NOW()
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8mb4