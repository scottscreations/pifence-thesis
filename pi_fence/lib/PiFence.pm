package PiFence;
use Mojo::Base 'Mojolicious';
use Mojo::mysql;

# This method will run once at server start
sub startup {
  my $self = shift;

  # Load configuration from hash returned by "my_app.conf"
  my $config = $self->plugin('Config');
  $self->helper(mysql => sub { state $mysql = Mojo::mysql->strict_mode('mysql://pifence:pifence@localhost/pifence')});
  # Documentation browser under "/perldoc"
  $self->plugin('PODRenderer') if $config->{perldoc};
  # Router
  my $r = $self->routes;
  
  # Normal route to controller
  $r->get('/passtest')->to('auth#passtest');
  
  
  $r->get('/')->to('dashboard#dashboard');
  $r->get('/dashboard/getcountries')->to('dashboard#getCountries');

  $r->get('/login')->to('auth#login');
  $r->post('/login')->to('auth#loginPost');

  $r->get('/traffic')->to('traffic#allTraffic');

  $r->post('/whitelist/add')->to('whitelist#postAdd');
  $r->post('/whitelist/modalAdd')->to('whitelist#modalAdd');
  $r->post('/whitelist/remove')->to('whitelist#remove');
  $r->get('/whitelist')->to('whitelist#list');

  $r->get('/settings')->to('settings#list');
}

1;
