package PiFence::Models::Auth;
use Mojo::Base -base;

use Mojo::Util 'secure_compare';


sub test {
	my $self = shift;

}

sub check {
	my ($self, $redis, $user, $pass) = @_;

    #$self->redis->hset(users => 'scott','pifence');
    my $stored_pass = $redis->hget("users" => $user);

	if($stored_pass && secure_compare $pass, $stored_pass){

		# Success
		# set session
        my $sessionData = {'username' => $user};
		return $sessionData;
	}

	# Fail
	return undef;
}

1;