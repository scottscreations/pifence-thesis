package PiFence::Controller::Dashboard;
use Mojo::Base 'Mojolicious::Controller';
use JSON::XS;
use Data::Structure::Util qw( unbless );
# This action will render a template
sub dashboard {
  my $self = shift;
  
  my $db = $self->mysql->db;
  my $results = $db->query('select dst_geo from traffic');
  my %countryHash;
  while (my $n = $results->hash) {
    if($n->{'dst_geo'})
    {
      my $coder = JSON::XS->new->ascii->pretty->allow_nonref;
      #print $n->{'dst_geo'}."\n";
      my $newData = $coder->decode ($n->{'dst_geo'});
      #print $newData->{'country_name'}."\n";
      my $country = $newData->{'country_code'} || "";
      if($countryHash{$country})
      {
        $countryHash{$country}->{'value'} = $countryHash{$country}->{'value'}+1;
      }
      else
      {
        $countryHash{$country}->{'value'} = 1;
        $countryHash{$country}->{'color'} = "#f56954";
        $countryHash{$country}->{'highlight'} = "#f56954";
        $countryHash{$country}->{'label'} = $country;
      }
    }
  }

  my $pieOptions;
  my $pieLabels;
  my $pieStats;
  for my $c (keys %countryHash) {
    use Data::Dumper;
    $pieOptions .= "{
      value    : $countryHash{$c}->{'value'},
      color    : '$countryHash{$c}->{'color'}',
      highlight: '$countryHash{$c}->{'highlight'}',
      label    : '$countryHash{$c}->{'label'}'
    },";
    $pieLabels .= "<li><i class=\"fa fa-circle-o text-red\"></i>flaghere $countryHash{$c}->{'label'}</li>";
    $pieStats .= "<li><a href=\"#\">$countryHash{$c}->{'label'}<span class=\"pull-right text-red\"><i class=\"fa fa-angle-down\"></i> $countryHash{$c}->{'value'}</span></a></li>";
    
  }
  $self->stash(pieCountryOptions => $pieOptions);
  $self->stash(pieCountryLabels => $pieLabels);
  $self->stash(pieCountryStats => $pieStats);

  



  $self->render('dashboard/dashboard');
}

sub getCountries
{
  my $self = shift;

}
1;
