package PiFence::Controller::Auth;
use Mojo::Base 'Mojolicious::Controller';
use PiFence::Models::Auth;

# This action will render a template
sub passtest {
    my $self = shift;
    my $auth = PiFence::Models::Auth->new();
    my $config = $self->config;
    say $config->{passSalt};
    $self->render(text => "Config here");
}

sub login {
	my $self = shift;

	# Render template "example/welcome.html.ep" with message
	$self->render();
}

sub loginPost{
    my $self = shift;
    my $auth = PiFence::Models::Auth->new();
    my $res = $auth->check($self->redis, $self->param('username'), $self->param('password'));
    if($res)
    {
        $self->session($res);
        warn "POST: ".$res->{'username'};
        $self->redirect_to('/');
    }
    else
    {
        $self->render(text => "Failed Login");
    }
}

1;
