package PiFence::Controller::Settings;
use Mojo::Base 'Mojolicious::Controller';

# This action will render a template
sub list {
  my $self = shift;
  
  $self->render();
}

1;
