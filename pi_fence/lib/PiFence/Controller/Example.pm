package PiFence::Controller::Example;
use Mojo::Base 'Mojolicious::Controller';

# This action will render a template
sub welcome {
  my $self = shift;
  $self->session('username' => 'scott');
  if(!$self->session('username'))
  {
    warn $self->session('username');
    $self->redirect_to('/login');
  }
  else
  {
    warn $self->session('username');
  }
  # Render template "example/welcome.html.ep" with message
  $self->render();
}

1;
