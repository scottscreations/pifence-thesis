package PiFence::Controller::Whitelist;
use Mojo::Base 'Mojolicious::Controller';
use JSON::XS;
use Geo::IP;
use Data::Structure::Util qw( unbless );
# This action will render a template

sub remove {
  my $self = shift;
  my $ip = $self->param('ip');
  my $dns = $self->param('dns');
  my $db = $self->mysql->db;
  $db->query('delete from whitelist where ip = TRIM(INET_ATON(?)) or dns = ?', $ip, $dns);
  $self->render(json => {"success" => 1});
  $self->render_later;
}

sub postAdd {
  my $self = shift;
  my $ip = $self->param('ip');
  my $db = $self->mysql->db;
  my $result = $db->query('select dst_dns, dst_geo from traffic where dst_ip = TRIM(INET_ATON(?))', $ip)->hash;
  $db->query('insert into whitelist (ip, dns, geo, created_on,type) values (TRIM(INET_ATON(?)), ?, ?, NOW(), \'IP\')', $ip, $result->{'dst_dns'}, $result->{'dst_geo'});
  $db->query('delete from traffic where dst_ip = (TRIM(INET_ATON(?)))', $ip);
  $self->render(json => {"success" => 1});
  $self->render_later;
}

sub modalAdd {
  my $self = shift;
  my $ip = $self->param('ip');
  my $dns = $self->param('dns');
  my $db = $self->mysql->db;
  if(defined($ip) && length($ip) > 0)
  {
    use Socket;
    my $iaddr = inet_aton($ip); # or whatever address
    my $dnsname  = gethostbyaddr($iaddr, AF_INET) || "";
    $self->flash("added");

    my $gi = Geo::IP->open("/usr/share/GeoIP/GeoIPCity.dat", GEOIP_STANDARD);

    my $record = $gi->record_by_addr($ip);
    if($record)
    {
      unbless($record);
      my $geoData = encode_json($record);
      #print Dumper($geoData);
      $db->query('insert into whitelist (ip, dns, geo, created_on,type) values (TRIM(INET_ATON(?)), ?, ?, NOW(), \'IP\')', $ip, $dnsname, $geoData);
    }
  }
  elsif(defined($dns) && length($dns) > 0)
  {
    $db->query('insert into whitelist (dns, created_on,type) values (?, NOW(), \'DNS\')', $dns);
    $self->flash("added");
  }
  $self->flash("failadded");

  $self->redirect_to('/whitelist');
}

sub list {
  my $self = shift;
  my $db = $self->mysql->db;
  my $results = $db->query('select created_on, INET_NTOA(ip) as ip, dns, geo, type from whitelist');
  my @newResults;
  while (my $n = $results->hash) {
    my $geoHTML = "";
    my $country = "";
    if($n->{'geo'})
    {
      my $coder = JSON::XS->new->ascii->pretty->allow_nonref;
      #print $n->{'dst_geo'}."\n";
      my $newData = $coder->decode ($n->{'geo'});
      #print $newData->{'country_name'}."\n";
      $country = $newData->{'country_code'} || "";
      my $city = $newData->{'city'} || "";
      my $region = $newData->{'region'} || "";
      $geoHTML = "<ul class=\"list-unstyled\">
          <li><strong>Country:</strong> $country</li>
          <li><strong>Region:</strong> $region</li>
          <li><strong>City:</strong> $city</li>
        </ul>";
    }
    my $data = {
    'created_on' => $n->{'created_on'},
    'ip' => $n->{'ip'},
    'dns' => $n->{'dns'},
    'geo' => $geoHTML,
    'country' => $country,
    'type' => $n->{'type'},
    };
    push @newResults, $data;
  }
 
  #    my $geoData = $t->{'dst_geo'};
      #warn Dumper($geoData);
      #my $geo = decode_json $geoData;
      #warn Dumper($geo);
  #}
  $self->stash(whitelistData => \@newResults);
  $self->render('whitelist/list');
}
1;
