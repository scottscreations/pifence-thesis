package PiFence::Controller::Traffic;
use Mojo::Base 'Mojolicious::Controller';
use JSON::XS;
use Data::Dumper;


# This action will render a template
sub welcome {
  my $self = shift;
  $self->session('username' => 'scott');
  if(!$self->session('username'))
  {
    warn $self->session('username');
    $self->redirect_to('/login');
  }
  else
  {
    warn $self->session('username');
  }
  # Render template "example/welcome.html.ep" with message
  $self->render();
}

sub allTraffic {
  my $self = shift;
  my $db = $self->mysql->db;
  my $results = $db->query('select timestamp, INET_NTOA(src_ip) as src_ip, INET_NTOA(dst_ip) as dst_ip, dst_dns, dst_port, protocol, dst_geo, counter from traffic');
  my @newResults;
  while (my $n = $results->hash) {
    my $geoHTML = "";
    my $country = "";
    if($n->{'dst_geo'})
    {
      my $coder = JSON::XS->new->ascii->pretty->allow_nonref;
      #print $n->{'dst_geo'}."\n";
      my $newData = $coder->decode ($n->{'dst_geo'});
      #print $newData->{'country_name'}."\n";
      $country = $newData->{'country_code'} || "";
      my $city = $newData->{'city'} || "";
      my $region = $newData->{'region'} || "";
      $geoHTML = "<ul class=\"list-unstyled\">
          <li><strong>Country:</strong> $country</li>
          <li><strong>Region:</strong> $region</li>
          <li><strong>City:</strong> $city</li>
        </ul>";
    }
    my $data = {
    'timestamp' => $n->{'timestamp'},
    'src_ip' => $n->{'src_ip'},
    'dst_ip' => $n->{'dst_ip'},
    'dst_dns' => $n->{'dst_dns'},
    'dst_port' => $n->{'dst_port'},
    'protocol' => $n->{'protocol'},
    'dst_geo' => $geoHTML,
    'counter' => $n->{'counter'},
    'dst_country' => $country,
  };


    push @newResults, $data;
  }
 
  #    my $geoData = $t->{'dst_geo'};
      #warn Dumper($geoData);
      #my $geo = decode_json $geoData;
      #warn Dumper($geo);
  #}
  $self->stash(trafficData => \@newResults);
  $self->render('traffic/traffic');
}

1;
