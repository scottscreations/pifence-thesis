
#/usr/lib/perl
use strict;
use warnings;
use NetAddr::IP;
use Data::Dumper;
use lib qw(..);

use JSON::XS qw( );

my $filename = 'ip-ranges.json';

my $json_text = do {
   open(my $json_fh, "<:encoding(UTF-8)", $filename)
      or die("Can't open \$filename\": $!\n");
   local $/;
   <$json_fh>
};

my $json = JSON::XS->new;
my $data = $json->decode($json_text);
#print Dumper($data);
for ( @{$data->{'prefixes'}} ) {
    if(new NetAddr::IP->new($_->{'ip_prefix'})->contains(new NetAddr::IP->new('8.8.8.8')))
    {
        print "===YES===\n";
    }
  # print $_->{'ip_prefix'}."\n";
}


my $amazonIP = new NetAddr::IP->new('52.84.0.0/15');
my $testIP = new NetAddr::IP->new('205.125.62.1/15');
if($amazonIP->contains($testIP))
{
    print "YES";
}
=head
my $subnet = NetAddr::IP->new($value->{network}, $value->{cidr});
while (my ($sub, $value) = each %{$subnets})
{
    if($subnet->contains($userIP))
    {
        if($lv->{'id'} == $value->{'site_name'})
        {
            $lv->{'selected'} = 1;
        }
    }
}
=cut



#my $coder = JSON::XS->new->ascii->pretty->allow_nonref;
#my $json  = $coder->decode($amazonData);

#print Data::Dumper($json);